import * as actionTypes from '../action/actionTypes';

const initialState = {
  posts: [],
  error:false,
  pages:1,
  loading:false

};

const reducer = (state = initialState, action) => {
	switch (action.type) 
	{
	case actionTypes.SET_API:
	return{
			...state,
			posts:action.posts
	}
    case actionTypes.GET_API_START:
      return {
        ...state,
        error: null,
        loading:true
      };
    case actionTypes.GET_API_SUCCESS:
    console.log(action.pages);
    return{
    	...state,
      posts:action.posts,
      error:null,
      loading:false,
      pages:action.pages

    };
    case actionTypes.GET_API_FAIL:
    return{
      ...state,
      error:action.error,
      loading:false

    };
    default:
      return state;
  }
};
export default reducer;