import * as actionTypes from './actionTypes';
import axios from 'axios';
import {URL} from '../../Constants/Url';

export const getApiSuccess = ( posts, pages) => {
  return {
    type: actionTypes.GET_API_SUCCESS,
    posts:posts,
    pages:pages
  };
};

export const getApiFail = error => {
  return {
    type: actionTypes.GET_API_FAIL,
    error:error
  };
};
export const getApiStart =() =>
{
	return{
		type : actionTypes.GET_API_START
	};
};

export const setApi = ( posts) => {
  return {
    type: actionTypes.SET_API,
    posts:posts
  };
};

export const getApi = () => {
	
  return dispatch => {
  	const queryParams = '?limit=10&page=1'
    axios.get(URL + queryParams)
         .then(response=>
           {
           	console.log(response.data);
              dispatch(setApi(response.data));
           })
           .catch(error=>
               {
               	console.log(error);
               	dispatch(getApiFail(error));
                   
               });
  };
};

export const getApiPages = (id, pages) => {
    return dispatch => {
        dispatch(getApiStart());
        const updatedPageNo = id === "prev" ? pages - 1 : pages + 1;
        const queryParams = '?limit=10&page=' + updatedPageNo;
        axios.get(URL + queryParams)
        .then(res => {
        	console.log(res.data, updatedPageNo);
            dispatch(getApiSuccess(res.data, updatedPageNo))
        })
        .catch(error => {
            dispatch(getApiFail(error));
        })
    };
}
