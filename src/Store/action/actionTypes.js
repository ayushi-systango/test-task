//data fetchingggg

export const SET_API='SET_API';
export const GET_API_START='GET_API_START';
export const GET_API_SUCCESS='GET_API_SUCCESS';
export const GET_API_FAIL='GET_API_FAIL';

// model open close

export const SET_MODAL='SET_MODAL';
export const SET_MODAL_OPEN_SUCCESS='SET_MODAL_OPEN_SUCCESS';
export const SET_MODAL_OPEN_FAIL='SET_MODAL_OPEN_FAIL';
export const SET_MODAL_CLOSE_SUCCESS='SET_MODAL_CLOSE_SUCCESS';
export const SET_MODAL_CLOSE_FAIL='SET_MODAL_CLOSE_FAIL'
