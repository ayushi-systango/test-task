import React, { Component } from 'react';
import { Modal , Button } from 'react-bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';

  class Model extends Component {
    render(){
      return(
        <Modal show={this.props.isOpen} onHide={this.props.closeModel}>
        <Modal.Header>
        <Modal.Title>Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <ul>
        <li>{this.props.selectRow && <span><strong>"User ID" : </strong>{this.props.selectRow.id}</span>}</li>
        <li>{this.props.selectRow && <span><strong>"Name" : </strong>{this.props.selectRow.name}</span>}</li>
        <li>{this.props.selectRow && <span><strong>"Price" : </strong>{this.props.selectRow.price}</span>}</li>
        <li>{this.props.selectRow && <span><strong>"Created At" : </strong>{this.props.selectRow.createdAt}</span>}</li>
        <li>{this.props.selectRow && <span><strong>"URL" : </strong>{this.props.selectRow.avatar}</span>}</li>
        </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.closeModel}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      )
    }
  }

  export default Model;
