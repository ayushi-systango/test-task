import React, { Component } from 'react';
import axios from 'axios';
import ReactTable from 'react-table-6';
import 'react-table-6/react-table.css';
import ReactDOM from "react-dom";
import { Button, ButtonGroup } from 'react-bootstrap';
import{connect} from 'react-redux';

import Model from './Model';
import Header from './UI/Header';
import Footer from './UI/Footer';

import { URL } from '../Constants/Url';
import { columns,defaultPageSize } from '../Constants/Data';
import {getApi , getApiPages} from '../Store/action/action';
import {getSelectRow , setModalCloseSuccess} from '../Store/action/modalAction';

import './PostList.css';


class PostList extends Component {

    componentDidMount()
    {
      this.props.onGetApi();
    }
    
    onRowClick=(data,props) => 
    {
      return{
        onClick:() =>{
            this.props.onGetSelectRow(data,props);
        }
      }
    }
    showRow=(data)=>
    {
      this.setState({isModelOpen:true, selectRow:data});
    }

    closeModel=()=>
    {
      this.props.onSetModalClose();
    }

    getPage = (event) => 
    {
      this.props.onGetApiPages(event.target.id, this.props.pages)
    }
    render()
    {
        const{posts , pages,isModelOpen,selectRow}=this.props;
        let data = posts;
         return (
          <div>
          <div className="App">
          <div className="container mt-2">
           <Header/>
          <h1 className="text-info font-weight-bold">Test Task</h1>
          {
            this.props.posts.length > 0 ?
            <ReactTable 
                data={data}  
                columns={columns}
                showPaginationBottom={true}
                pages={10}
                showPagination={false}   
                filterable 
                getTrProps={this.onRowClick}
                defaultPageSize={defaultPageSize}
                className="-striped -highlight"/>
                :
                <div className="spinner-border text-muted"></div> 
            }
              <Model 
                isOpen={isModelOpen}
                showRow={this.onRowClick}
                selectRow={selectRow} 
                closeModel={this.closeModel}
              />
            </div>
            </div>
            <div className="d-inline-flex mb-3">
            <ButtonGroup className="mr-2" aria-label="First group">
            <Button 
                id="prev"
                type='prev'
                onClick={this.getPage} 
                disabled={pages<=1}>
                Previous
            </Button>
            <Button variant="light">
                {pages === "0" ? "1" : pages + " / 5"}
            </Button>{' '}
            <Button 
                id="next"
                type='next'
                onClick={this.getPage} 
                disabled={pages>=5}>
                Next
            </Button>
            </ButtonGroup>
            </div>
            <Footer/>
            </div>
       );
    }
}
const mapStateToProps = state =>{
    return{
        posts:state.postApi.posts,
        pages:state.postApi.pages,
        isModelOpen:state.modal.isModelOpen,
        selectRow:state.modal.selectRow
    };
}
const mapDispatchToProps = dispatch=>
{
    return{
        onGetApi:() =>dispatch(getApi()),
        onGetApiPages: (id, pages) => dispatch(getApiPages(id,pages)),
        onGetSelectRow:(data,props)=> dispatch(getSelectRow(data,props)),
        onSetModalClose:()=> dispatch(setModalCloseSuccess())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PostList);
