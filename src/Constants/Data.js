export const columns = [
          {
            Header: 'User ID',  
            accessor:'id',
            width:100  
          },  
          {
            Header: 'Name',  
            accessor: 'name',
            width:100,
            sortable:false 
          },
          {
            Header: 'Price',  
            accessor: 'price',
            width:70,
            sortable:false 
          },    
          {
            Header: 'Created At',  
            accessor: 'createdAt',
            sortable:false 
          },
          
          {
            Header: 'Color',  
            accessor: 'color',
            width:100 ,
            sortable:false     
          },
          {
            Header: 'Availability',  
            accessor: 'availability',
            width:120,
            sortable:false 
          },
          {
            Header: 'Dimension',  
            accessor: 'dimension',
            width:120,
            sortable:false 
          },
          {
            Header: 'URL',  
            accessor: 'avatar' ,
            sortable:false     
          }
        ];
        
export const defaultPageSize = 10;   
