import React from 'react';
import PostList from './Components/PostList';
import './App.css';

function App() {
  return (
    <div className="App">
      <PostList/>
    </div>
  );
}

export default App;
